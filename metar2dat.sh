#!/bin/bash
read METAR
LETISKO=$(echo "$METAR" | grep -o "[0-9]\{12\}.[A-Z]\{5\}.[A-Z]\{4\}" | cut -c 20-)
printf "date time $LETISKO\n"
ROK=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c-4)
MESIAC=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c 5- | cut -c-2)
DEN=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c 7- | cut -c-2)
HODINY=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c 9- | cut -c-2)
MINUTY=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c 11- | cut -c-2)
TEPLOTA=$(echo "$METAR" | sed "s@.*\(M\{0,1\}[0-9]\{2\}\)/.*@\1@" | tr -s 'M' '-')
printf "$ROK-$MESIAC-$DEN $HODINY:$MINUTY $TEPLOTA\n"
while read METAR
do
  ROK=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c-4)
  MESIAC=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c 5- | cut -c-2)
  DEN=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c 7- | cut -c-2)
  HODINY=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c 9- | cut -c-2)
  MINUTY=$(echo "$METAR" | grep -o "[0-9]\{12\}" | cut -c 11- | cut -c-2)
  TEPLOTA=$(echo "$METAR" | sed "s@.* \(M\{0,1\}[0-9]\{2\}\)/.*@\1@" | tr 'M' '-')
  printf "$ROK-$MESIAC-$DEN $HODINY:$MINUTY $TEPLOTA\n"
done
