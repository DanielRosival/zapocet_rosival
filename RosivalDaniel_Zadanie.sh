#!/bin/bash 
AIRPORT=$(echo "$1" | grep '^[A-Z]\{4\}$')
DATUM=$(echo "$2" | grep '[0-3][0-9]\-[0-1][0-9]\-[0-9]\{4\}')
if [[ -z "$AIRPORT" ]]; then
	printf "Wrong arguments"
	exit 1
fi
if [[ -z "$DATUM" ]]; then
	DATUM=`date "+%d-%m-%Y"`
fi
DEN=$(echo "$DATUM" | sed 's/\-.*//')
MESIAC=$(echo "$DATUM" | sed 's/\([0-9]\{2\}\-\)\([0-9]\{2\}\)\(\-[0-9]\{4\}\)/\2/')
ROK=$(echo "$DATUM" | sed 's/\([0-9]\{2\}\-\)\([0-9]\{2\}\-\)\([0-9]\{4\}\)/\3/')
curl --silent 'http://www.ogimet.com/display_metars2.php?lang=en&lugar='$AIRPORT'&tipo=ALL&ord=REV&nil=SI&fmt=txt&ano='$ROK'&mes='$MESIAC'&day='$DEN'&hora=00&anof='$ROK'&mesf='$MESIAC'&dayf='$DEN'&horaf=23&minf=59&send=send' | tr -d '\n' | sed 's/ \{1,\}/ /g' | sed 's/\=/&\n/g' | sed 's/\#/&\n/g' | grep '[0-9].METAR'